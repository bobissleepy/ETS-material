/*
 * File           : $Header: //depot/main/template-kit/template-kit-publication/src/main/webapp/template/version/javascript/formValidator.js#11 $
 * Last edited by : $Author: shud $ $Date: 2008/02/28 $
 * Version        : $Revision: #11 $
 *
 * Copyright (C) 2007 Escenic AS.
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from Escenic AS.  This notice must be included on
 * all copies, modifications and derivatives of this work.
 */

var positiveIntegerFilter = /^\d+$/;
var emailAddressFilter = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;
var namevalid=/^([a-zA-Z ])+([\. a-zA-Z])+$/;

function isEmpty(form, fieldName) {
  var value = form[fieldName].value;
  return value == null || value == "";
}

function validatePositiveInteger(form, fieldName, spanId, errorMessage) {
  return validateField(form, fieldName, spanId, positiveIntegerFilter, errorMessage)
}

/*function validateEmailAddress(form, fieldName, spanId, errorMessage) {
  return validateField(form, fieldName, spanId,
      emailAddressFilter, errorMessage)
}*/
function validateName(form, fieldName, spanId, errorMessage) {
	  return validateField(form, fieldName, spanId,
	      namevalid, errorMessage)
	}

function validateField(form, fieldName, spanId, filter, errorMessage) {
  var field = form[fieldName];
  var value = field.value;

  if (filter.test(value)) {
    return true;
  }

  field.focus();
  var errorSpan = document.getElementById(spanId);
  errorSpan.innerHTML = errorMessage;
  errorSpan.className = "error";
  field.style.border = '2px solid red';
  return false;
}

function validateEmptyField(form, fieldName, spanId, errorMessage) {

  var field = form[fieldName];
  var errorSpan = document.getElementById(spanId);

  if (isEmpty(form,fieldName)) {
    field.focus();
    errorSpan.innerHTML = errorMessage;
    errorSpan.className = "error";
    field.style.border = '2px solid red';
    return false;
  } else {
    return true;
  }
}

function revertToNormalField(form, fieldName, spanId) {
  var field = form[fieldName];
  var errorSpan = document.getElementById(spanId);
  if (errorSpan) {
    errorSpan.innerHTML = "";
    //field.style.border = null;
  }
}

function validateCommentForm(form, nameErrorMessage, bodyErrorMessage) {
  var success = true;

  if (validateEmptyField(form, 'name', 'forum.comment.error', nameErrorMessage)) {
    revertToNormalField(form, 'name', 'forum.comment.name.error');
  } else {
    success = false;
  }

 
  if (validateEmptyField(form, 'body', 'forum.comment.error', bodyErrorMessage)) {
    revertToNormalField(form, 'body', 'forum.comment.error');
  } else {
    success = false;
  }
  if (validateName(form, 'name', 'forum.comment.error', nameErrorMessage)) {
	    revertToNormalField(form, 'name', 'forum.comment.name.error');
	  } else {
	    success = false;
	  }

  return success;
}
